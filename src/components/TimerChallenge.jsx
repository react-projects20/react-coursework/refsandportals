import { useRef, useState } from 'react';
import ResultModal from './ResultModal';


export default function TimerChallenge({title, targetTime}) {
    const timer = useRef()
    const dialog = useRef()

    const [timeRemaining, settimeRemaining] = useState(targetTime * 1000);

    // const [timerStarted, setTimerStarted] = useState(false);
    // const [timerExpired, setTimerExpired] = useState(false);

    const timerIsActive = timeRemaining > 0 && timeRemaining < targetTime * 1000

    if (timeRemaining <=0) {
        clearInterval(timer.current);
        //settimeRemaining(targetTime * 1000);
        dialog.current.open();
    }

    function handleReset(params) {
        settimeRemaining(targetTime * 1000);
    }

    function handleStart() {
        
        timer.current = setInterval(()=> {
            settimeRemaining(prevTimeRemaining => prevTimeRemaining - 10)
            // setTimerExpired(true)
            // dialog.current.open();
        },10);

        // setTimerStarted(true);
    }

    function handleStop() {
        clearInterval(timer.current);
        dialog.current.open();
    }

    return (
        <>
        <ResultModal ref={dialog} targetTime={targetTime} remainingTime={timeRemaining} onReset={handleReset}/>
        <section className="challenge">
            <h2>{title}</h2>
           
            <p className="challenge-time">
                {targetTime} second{targetTime > 1 ? 's' : ''}
            </p>
            <p>
                <button onClick={timerIsActive ? handleStop :  handleStart}>
                    {timerIsActive ? 'Stop': 'Start'} Challenge
                </button>
            </p>
            <p className={timerIsActive ? 'active' : undefined}>
                {timerIsActive ? 'Timer is running...' : 'Timer has ended...'}

            </p>
        </section>
        </>
    )
}